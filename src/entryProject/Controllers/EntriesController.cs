using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using entryProject.Models.DbModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace entryProject.Controllers
{
    public class EntriesController : Controller
    {
        aspnet_entryProject_1ec08db8_47b0_4fae_82d7_4da8440d3aa6Context dataSource = new aspnet_entryProject_1ec08db8_47b0_4fae_82d7_4da8440d3aa6Context();
        public IActionResult Index()
        {
            var outputModel = dataSource.TbEntries.Include(x => x.IdCardNavigation)
                .Include(x => x.IdIssuerNavigation);
            return View(outputModel);
        }

        [Authorize]
        public async Task<IActionResult> checkIn(long id)
        {
            var card = dataSource.CsCard.FirstOrDefault(x => x.IdCard == id);
            if (card != null)
                if (card.Active)
                {
                    var entryList = dataSource.TbEntries.Where(x => x.IdCard == id).OrderByDescending(x=>x.Datetime).FirstOrDefault();
                    if (entryList != null)
                    {
                        if(entryList.IsCheckedIn)
                            return StatusCode(StatusCodes.Status400BadRequest, "Visitor is already checked-in!");
                        else
                            try
                            {
                                await checkInCard(id);
                            }
                            catch (Exception)
                            {
                                return StatusCode(StatusCodes.Status400BadRequest, "Error saving changes! Contact admin!");
                            }
                    }
                    else
                        try
                        {
                            await checkInCard(id);
                        }
                        catch (Exception)
                        {
                            return StatusCode(StatusCodes.Status400BadRequest, "Error saving changes! Contact admin!");
                        }
                }
                else
                    return StatusCode(StatusCodes.Status400BadRequest, "This card is inactive!");
            else
                return StatusCode(StatusCodes.Status400BadRequest, "No such card ID!");

            var outputModel = dataSource.TbEntries.Include(x => x.IdCardNavigation)
               .Include(x => x.IdIssuerNavigation);

            return PartialView("EntriesPartial", outputModel);
        }

        [Authorize]
        public async Task<IActionResult> checkOut(long id)
        {
            var card = dataSource.CsCard.FirstOrDefault(x => x.IdCard == id);
            if (card != null)
                if (card.Active)
                {
                    var entryList = dataSource.TbEntries.Where(x => x.IdCard == id).LastOrDefault();
                    if (entryList != null)
                    {
                        if (!entryList.IsCheckedIn)
                            return StatusCode(StatusCodes.Status400BadRequest, "Visitor is already checked-out!");
                        else
                        {
                            try
                            {
                                await checkOutCard(id);
                            }
                            catch (Exception)
                            {
                                return StatusCode(StatusCodes.Status400BadRequest, "Error saving changes! Contact admin!");
                            }
                        }
                    }
                    else
                        try
                        {
                            await checkOutCard(id);
                        }
                        catch (Exception)
                        {
                            return StatusCode(StatusCodes.Status400BadRequest, "Error saving changes! Contact admin!");
                        }
                }
                else
                    return StatusCode(StatusCodes.Status400BadRequest, "This card is inactive!");
            else
                return StatusCode(StatusCodes.Status400BadRequest, "No such card ID!");

            var outputModel = dataSource.TbEntries.Include(x => x.IdCardNavigation)
               .Include(x => x.IdIssuerNavigation);

            return PartialView("EntriesPartial", outputModel);
        }

        private async Task checkInCard(long id)
        {
            try
            {
                dataSource.TbEntries.Add(new TbEntries
                {
                    IdCard = id,
                    IdIssuer = dataSource.AspNetUsers.First(x => x.UserName == User.Identity.Name).Id,
                    IsCheckedIn = true,
                    Datetime = DateTime.Now
                });

                await dataSource.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        private async Task checkOutCard(long id)
        {
            try
            {
                dataSource.TbEntries.Add(new TbEntries
                {
                    IdCard = id,
                    IdIssuer = dataSource.AspNetUsers.First(x => x.UserName == User.Identity.Name).Id,
                    IsCheckedIn = false,
                    Datetime = DateTime.Now
                });
                await dataSource.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }
    }

}