using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using entryProject.Models.DbModels;
using Microsoft.AspNetCore.Http;

namespace entryProject.Controllers
{
    public class CardsController : Controller
    {

        // NOTICE:
        /*
         * Ive create a "traditional" view oriented controller for this purpose because it goes nicely with ajax for partial views refresh that i used on the FE.
         * If i would be using angular I would have created an API style controller.
         * 
         * */

        aspnet_entryProject_1ec08db8_47b0_4fae_82d7_4da8440d3aa6Context dataSource = new aspnet_entryProject_1ec08db8_47b0_4fae_82d7_4da8440d3aa6Context();
        [Authorize]
        public IActionResult Index()
        {
            return View(dataSource.CsCard);
        }

        [Authorize]
        public async Task<IActionResult> createVisitorsCard([FromForm]CsCard card)
        {
            if(ModelState.IsValid)
            {
                if (card.Name != null && card.Surname != null)
                {
                    if (dataSource.CsCard.FirstOrDefault(x => x.Name == card.Name && x.Surname == card.Surname && x.Active == true) == null)
                    {
                        dataSource.CsCard.Add(card);
                        try
                        {
                            await dataSource.SaveChangesAsync();
                        }
                        catch (Exception)
                        {
                            return StatusCode(StatusCodes.Status500InternalServerError, "Unable to save data. Contact admin!");
                        }
                    }
                    else
                        return StatusCode(StatusCodes.Status409Conflict, "Visitor with this name already exists");
                }
                else
                    return StatusCode(StatusCodes.Status400BadRequest, "Name and surname are required!");
            }

            return PartialView("CardsPartial", dataSource.CsCard);
        }

        [Authorize]
        public async Task<IActionResult> deleteVisitorsCard(long id)
        {
            var card = dataSource.CsCard.FirstOrDefault(x => x.IdCard == id);
            if(card!=null)
                if(card.Active)
                    return StatusCode(StatusCodes.Status400BadRequest, "Visitor is checked-in!"); 
            else
                {
                    dataSource.Remove(dataSource.CsCard.Where(x => x.IdCard == id).First());
                    await dataSource.SaveChangesAsync();
                }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest, "No such card ID!");
            }

            return PartialView("CardsPartial", dataSource.CsCard);
        }

        [Authorize]
        public async Task<IActionResult> deactivate(long id)
        {
            var card = dataSource.CsCard.FirstOrDefault(x => x.IdCard == id);
            if (card != null)
                if (!card.Active)
                    return StatusCode(StatusCodes.Status400BadRequest, "Visitor is already inactive");
                else
                {
                    dataSource.CsCard.Where(x => x.IdCard == id).First().Active = false;
                    await dataSource.SaveChangesAsync();
                }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest, "No such card ID!");
            }

            return PartialView("CardsPartial", dataSource.CsCard);
        }
        [Authorize]
        public async Task<IActionResult> activate(long id)
        {
            var card = dataSource.CsCard.FirstOrDefault(x => x.IdCard == id);
            if (card != null)
                if (card.Active)
                    return StatusCode(StatusCodes.Status400BadRequest, "Visitor is already active");
                else
                {
                    dataSource.CsCard.Where(x => x.IdCard == id).First().Active = true;
                    await dataSource.SaveChangesAsync();
                }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest, "No such card ID!");
            }

            return PartialView("CardsPartial", dataSource.CsCard);
        }

       
    }
}