﻿using System;
using System.Collections.Generic;

namespace entryProject.Models.DbModels
{
    public partial class CsCard
    {
        public CsCard()
        {
            TbEntries = new HashSet<TbEntries>();
        }

        public long IdCard { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<TbEntries> TbEntries { get; set; }
    }
}
