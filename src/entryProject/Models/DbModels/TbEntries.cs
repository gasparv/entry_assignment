﻿using System;
using System.Collections.Generic;

namespace entryProject.Models.DbModels
{
    public partial class TbEntries
    {
        public long IdEntry { get; set; }
        public string IdIssuer { get; set; }
        public long IdCard { get; set; }
        public bool IsCheckedIn { get; set; }
        public DateTime Datetime { get; set; }

        public virtual CsCard IdCardNavigation { get; set; }
        public virtual AspNetUsers IdIssuerNavigation { get; set; }
    }
}
