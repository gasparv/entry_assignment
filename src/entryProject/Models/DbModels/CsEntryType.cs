﻿using System;
using System.Collections.Generic;

namespace entryProject.Models.DbModels
{
    public partial class CsEntryType
    {
        public long IdEntryType { get; set; }
        public string EntryType { get; set; }
    }
}
