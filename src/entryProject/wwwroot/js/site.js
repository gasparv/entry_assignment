﻿function notify(message, type) {
    type === null ? type = "warning" : type = type;

    switch(type)
    {
        case "danger":
            {
                $.growl.error({
                    title: "",
                    message: message
                }, {
                    type: type,
                    allow_dismiss: false,
                    label: 'Close',
                    className: 'btn-xs btn-inverse',
                    delay: 2500,
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutDown'
                    }
                });
                break;
            }
        case "warning":
            {
                $.growl.warning({
                    title: "",
                    message: message
                }, {
                    type: type,
                    allow_dismiss: false,
                    label: 'Close',
                    className: 'btn-xs btn-inverse',
                    delay: 2500,
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutDown'
                    }
                });
                break;
            }
        case "success":
            {
                $.growl.notice({
                    title: "",
                    message: message
                }, {
                    type: type,
                    allow_dismiss: false,
                    label: 'Close',
                    className: 'btn-xs btn-inverse',
                    delay: 2500,
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutDown'
                    }
                });
                break;
            }
        default:
            {
                $.growl({
                    title: "",
                    message: message
                }, {
                    type: type,
                    allow_dismiss: false,
                    label: 'Close',
                    className: 'btn-xs btn-inverse',
                    delay: 2500,
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutDown'
                    }
                });
                break;
            }
    }
     
   
}